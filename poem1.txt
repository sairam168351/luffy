1. Poems awaken the dormant soul in us.
2.  We all come at place, where we feel dejected and disenfranchised with this materialistic world,
3.  once in our lives. At that time of undetermined condition and confusion,
4.  the poems will make our heart clear of all the debris and unwanted feelings
5.  and make our heart pure with the awakened soul. 
6. vamsi is watching